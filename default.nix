with import <nixpkgs> {};
with python3Packages;

buildPythonPackage rec {
  name = "greatsong-parser";
  src = ./.;
  propagatedBuildInputs = [
    aiohttp
    lxml
  ];
}
