#!/usr/bin/env python3

import asyncio
import base64
from io import BytesIO
import json
import os
import re
from urllib.parse import quote, urljoin

import aiohttp
from lxml import etree

B64_BASE_URL = "aHR0cDovL21hZ25hdHVuZS5jb20="
B64_TRACK_SERVER_URL = "aHR0cDovL2hlMy5tYWduYXR1bmUuY29tL211c2ljLw=="
BASE_URL = base64.b64decode(B64_BASE_URL).decode("utf-8")
TRACK_SERVER_URL = base64.b64decode(B64_TRACK_SERVER_URL).decode("utf-8")
WORKING_DIR = os.path.dirname(os.path.abspath(__file__))
TRACK_SEMAPHORE = asyncio.Semaphore(20)

def name2filename(name):
    """
    From a name meant to be displayed, returns a filename compatime name.
    """
    name = name.replace(" ", "_")
    name = name.replace("/", "-")
    # TODO replace parenthesis
    return name

class Endpoint:
    """
    Abstract HTTP Enpoint representation.
    """

    headers = {
        "User-Agent": (
            "Mozilla/5.0 (X11; Debian; Linux x86_64; rv:52.0) "
            "Gecko/20100101 Firefox/58.0"
        )
    }

    def __init__(self, url):
        self.url = url
        self.path = None

    async def get_content(self):
        """
        Returns an http response as a file object.
        """
        kwargs = {
            "headers": self.headers,
        }
        async with aiohttp.ClientSession() as session:
            async with session.get(self.url, **kwargs) as response:
                return await response.read()

    def parse(self):
        raise NotImplementedError

    def download(self):
        raise NotImplementedError


class CommonPage(Endpoint):
    """
    Abstract implementation of an HTML page.
    """

    def __init__(self, url):
        super().__init__(url)
        self.tree = None

    async def parse(self):
        self.tree = await self.get_tree()

    async def get_tree(self):
        """
        Returns an xml tree from an html http response.
        """
        with BytesIO(await self.get_content()) as response:
            parser = etree.HTMLParser()
            tree = etree.parse(response, parser)
        return tree

class ArtistPage(CommonPage):
    """
    Concrete implementation of an artist webpage.
    """

    url_suffix = "artists"
    xpath_album_url = '//a[contains(@href, "artists/albums")]'

    def __init__(self, artist_name):
        suffix = "/".join([self.url_suffix, artist_name])
        url = urljoin(BASE_URL, suffix)
        super().__init__(url)
        self.name = artist_name
        folder_name = name2filename(self.name)
        self.path = os.path.join(WORKING_DIR, folder_name)
        self.album_pages = []

    async def parse(self):
        await super().parse()
        self.album_pages = self.get_album_pages()
        tasks = [page.parse() for page in self.album_pages]
        return await asyncio.gather(*tasks)

    def get_album_urls(self):
        """
        Returns a list of album urls from an artist xml content.
        """
        urls = []
        for elem in self.tree.xpath(self.xpath_album_url):
            urls.append(urljoin(BASE_URL, elem.get("href")))
        return list(set(urls)) # Avoid duplicated urls


    def get_album_pages(self):
        """
        Returns a list of album pages instance.
        """
        urls = self.get_album_urls()
        return [AlbumPage(url, self) for url in urls]

    async def download(self):
        if not os.path.exists(self.path):
            os.mkdir(self.path)
        for album_page in self.album_pages:
            await album_page.download()


class AlbumPage(CommonPage):
    """
    Concrete album page implementation.
    """

    xpath_infos = "//head/script[contains(@src, '/js/data/albums')]"

    def __init__(self, url, artist_page):
        super().__init__(url)
        self.artist_page = artist_page
        self.information_page = None
        self.artist = None
        self.sku = None
        self.name = None
        self. tracks = None

    async def parse(self):
        await super().parse()
        self.information_page = await self.get_infos()
        self.artist = self.information_page.infos.get("artist")
        self.sku = self.information_page.infos.get("sku")
        self.name = self.information_page.infos.get("albumname")
        self.path = os.path.join(
            self.artist_page.path,
            name2filename(self.name)
        )
        self.tracks = self.get_tracks(
            self.information_page.infos.get("songs", [])
        )

    async def get_infos(self):
        """
        Gets the information page url and returns an InformationPage instance.
        """
        url_suffix = self.tree.xpath(self.xpath_infos)[0].get("src")
        url_infos = urljoin(BASE_URL, url_suffix)
        information_page = InformationPage(url_infos)
        await information_page.parse()
        return information_page

    def get_tracks(self, tracks_data):
        """
        Returns a list of track instances for each data passed as argument.
        """
        tracks = []
        for data in tracks_data:
            tracks.append(Track(self, data))
        return tracks

    async def download(self):
        """
        For each track url in the album page, download it and store it
        in the dedicated album folder.
        """
        if not os.path.exists(self.path):
            os.mkdir(self.path)
        print("Downloading {}".format(self.name))
        tasks = [track.download() for track in self.tracks]
        return await asyncio.gather(*tasks)

class InformationPage(CommonPage):
    """
    Concrete information page implementation.
    Warning : If something crashed, it probably comes from here.
    self.infos is supposed to be a json parsed from a JS file.
    I may have forgotten some chars to deal with in format_js_to_json.
    """

    def __init__(self, url):
        super().__init__(url)
        self.infos = None

    async def parse(self):
        raw_content = await self.get_content()
        raw_content = raw_content.decode("unicode_escape")
        self.infos = self.format_js_to_json(raw_content)

    def format_js_to_json(self, content):
        """
        Parse informations from a JS file into a json structure.
        There should be more rules to add if I haven't tried to
        download every album.
        """
        content = re.sub(r"\s", " ", content)
        content = re.sub(r"var\salbumdata.*?{", "{", content)
        content = content.replace(";", "") # Semicolons
        # Remove the whole album notes attribute, as it is full of
        # problematic chars
        content = re.sub(
            r'\"albumnotes.*?\",\s*\"',
            '"',
            content
        )
        return json.loads(content)

    def download(self):
        pass


class Track(Endpoint):
    """
    Concrete track file endpoint implementation.
    """

    def __init__(self, album_page, data):
        self.data = data
        self.album_page = album_page
        self.name = data.get("wav")
        url = self.format_url()
        super().__init__(url)
        self.filename = name2filename(self.name)
        self.path = os.path.join(album_page.path, self.filename)

    def format_url(self):
        """
        Join informations and quote them to make a valid url.
        """
        url = urljoin(
            TRACK_SERVER_URL,
            "/".join(
                [
                    self.album_page.artist,
                    self.album_page.name,
                    self.name
                ]
            )
        )
        return quote(url, safe="/:")

    def check_path(self):
        """
        Removes the file path is already in the FS.
        TODO : Maybe not what is wanted. Configuration ?
        When exists and update == True : remove file, return False
        When exists and update == False : return True
        Else : Return False
        """
        if os.path.exists(self.path):
            os.remove(self.path)
        return False

    async def download(self):
        if self.check_path():
            # File already exists, do nothing
            return
        async with TRACK_SEMAPHORE:
            print("Downloading track {}".format(self.name))
            content = await self.get_content()
            with open(self.path, "wb") as file_obj:
                file_obj.write(content)
            print("Track written in {}".format(self.path))

    def parse(self):
        pass

if __name__ == "__main__":
    NAME = "aitua"
    PAGE = ArtistPage(NAME)
    LOOP = asyncio.get_event_loop()
    LOOP.run_until_complete(PAGE.parse())
    LOOP.run_until_complete(PAGE.download())
